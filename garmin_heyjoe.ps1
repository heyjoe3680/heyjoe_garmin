$storageDir = "D:\garmin_heyjoe\"
$webclient = New-Object System.Net.WebClient
$url = "http://download.geofabrik.de/europe/hungary-latest.osm.pbf"
$file = "$storageDir\hungary.pbf"
Remove-Item $file
$webclient.DownloadFile($url,$file)

java -Xmx1024m -ea -jar D:\osmdaily\splitter.jar --mapid=71221559  --max-nodes=1600000 --max-areas=255 D:\garmin_heyjoe\hungary.pbf --output-dir=D:\garmin_heyjoe\args

Copy-Item D:\garmin_heyjoe\template.args D:\garmin_heyjoe\args\template.args
java -Xmx1024m -ea -jar D:\osmdaily\mkgmap.jar --mapname=71221559 --family-id=3680 D:\garmin_heyjoe\joe.typ  --overview-mapname=heyjoehu --series-name="heyjoehu"  --family-name="heyjoehu"    --remove-short-arcs --style-file=D:\garmin_heyjoe\styles --style=heyjoe --keep-going  --check-roundabouts  --drive-on=right  --output-dir=D:\garmin_heyjoe\temp  --index  --route  --nsis  --tdbfile  --description="heyjoehu"  --draw-priority=10  --add-pois-to-areas --lower-case --add-pois-to-lines --generate-sea=land-tag=natural=background --gmapsupp --code-page=1250 --show-profiles=1 -c D:\garmin_heyjoe\args\template.args


Copy-Item D:\garmin_heyjoe\heyjoe.nsi D:\garmin_heyjoe\temp\heyjoe.nsi
Copy-Item D:\garmin_heyjoe\icon.ico D:\garmin_heyjoe\temp\icon.ico
Copy-Item D:\garmin_heyjoe\joe.typ D:\garmin_heyjoe\temp\joe.typ
Copy-Item D:\garmin_heyjoe\heyjoe_license.txt D:\garmin_heyjoe\temp\heyjoe_license.txt

C:\PROGRA~2\NSIS\makensis.exe D:\garmin_heyjoe\temp\heyjoe.nsi