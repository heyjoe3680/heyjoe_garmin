!define DEFAULT_DIR "C:\Garmin\Maps\heyjoe.hu"
!define INSTALLER_DESCRIPTION "heyjoehu"
!define INSTALLER_NAME "heyjoehu"
!define MAPNAME "heyjoehu"
!define PRODUCT_ID "1"
!define REG_KEY "heyjoehu"
!define INDEX
!define TYPNAME "joe.typ"

SetCompressor /SOLID lzma

; Includes
!include "MUI2.nsh"
!define MUI_ICON "icon.ico"



; Installer pages
!define MUI_CUSTOMFUNCTION_GUIINIT myGuiInit
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE heyjoe_license.txt
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!define MUI_UNPAGE_INSTFILES

; Language files
!define MUI_LANGDLL_ALLLANGUAGES
!insertmacro MUI_LANGUAGE "Hungarian"
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "Spanish"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Dutch"

LangString AlreadyInstalled ${LANG_Hungarian} "${INSTALLER_NAME} m�r telep�tve van. $\n$\nKattints az 'OK' gombra az el�z� v�ltozat friss�t�s�hez. A 'Cancel' megszak�tja a telep�t�st"
LangString AlreadyInstalled ${LANG_ENGLISH} "${INSTALLER_NAME} is already installed. $\n$\nClick `OK` to remove the previous version and continue installation or `Cancel` to cancel this upgrade."
LangString AlreadyInstalled ${LANG_FRENCH} "${INSTALLER_NAME} est d�j� install�. $\n$\nAppuyez sur `OK` pour retirer la version pr�c�dente et continuer avec l'installation ou sur `Annuler` pour annuler cette mise � jour."
LangString AlreadyInstalled ${LANG_SPANISH} "${INSTALLER_NAME} ya est� instalado. $\n$\nPulse `Aceptar` para eliminar la versi�n anterior y continuar la instalaci�n o `Cancelar` para cancelar esta actualizaci�n."
LangString AlreadyInstalled ${LANG_GERMAN} "${INSTALLER_NAME} ist bereits installiert. $\n$\nKlick `OK` um die alte Version zu deinstallieren oder `Abbrechen` um die Installation abzubrechen."
LangString AlreadyInstalled ${LANG_DUTCH} "${INSTALLER_NAME} is reeds geinstalleerd. $\n$\nKlik op `OK` om de oude versie te verwijderen of `Annuleren` om deze update te onderbreken."

; Reservefiles
!insertmacro MUI_RESERVEFILE_LANGDLL ;Language selection dialog


Name "${INSTALLER_DESCRIPTION}"
OutFile "${INSTALLER_NAME}.exe"
InstallDir "${DEFAULT_DIR}"

Function .onInit
  !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

Function myGUIInit
  ;Read $INSTDIR from the registry
  ClearErrors
  ReadRegStr $INSTDIR HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "LOC"
  IfErrors +2
  StrCmp $INSTDIR "" 0 +2
  StrCpy $INSTDIR "${DEFAULT_DIR}"
  
  ; Uninstall before installing (code from http://nsis.sourceforge.net/Auto-uninstall_old_before_installing_new )
  ReadRegStr $R0 HKLM \
  "Software\Microsoft\Windows\CurrentVersion\Uninstall\${REG_KEY}" "UninstallString"
  StrCmp $R0 "" done
 
  IfSilent silent
  MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION "$(AlreadyInstalled)" IDOK uninst
  Abort

  ;Run the uninstaller
  uninst:
  ClearErrors
  ExecWait '"$R0" /S ' ;Do not copy the uninstaller to a temp file
 
  IfErrors no_remove_uninstaller done
    ;You can either use Delete /REBOOTOK in the uninstaller or add some code
    ;here to remove the uninstaller. Use a registry key to check
    ;whether the user has chosen to uninstall. If you are using an uninstaller
    ;components page, make sure all sections are uninstalled.
  no_remove_uninstaller:
  
  Goto done
 
  silent:
  ExecWait '"$R0" /S ' ;Do not copy the uninstaller to a temp file
 
  done:
 
FunctionEnd

Function un.onInit
!insertmacro MUI_UNGETLANGUAGE
FunctionEnd

Section "MainSection" SectionMain
; Files to be installed
  SetOutPath "$INSTDIR"
  File "${MAPNAME}.img"
  File "${MAPNAME}_mdr.img"
  File "${MAPNAME}.mdx"
  File "joe.typ"
  File "${MAPNAME}.tdb"
  File "71221559.img"
  File "71221560.img"
  File "71221561.img"
  File "71221562.img"
  File "71221563.img"
  File "71221564.img"
  File "71221565.img"
  File "71221566.img"
  File "71221567.img"
  File "71221568.img"
  File "71221569.img"
  File "71221570.img"
  File "71221571.img"
  File "71221572.img"
  File "71221573.img"

; Create MapSource registry keys
  WriteRegBin HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "ID" 600e
!ifdef INDEX  
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "IDX" "$INSTDIR\${MAPNAME}.mdx"
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "MDR" "$INSTDIR\${MAPNAME}_mdr.img"
!endif
!ifdef TYPNAME  
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "TYP" "$INSTDIR\${TYPNAME}"
!endif
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "BMAP" "$INSTDIR\${MAPNAME}.img"
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "LOC" "$INSTDIR"
  WriteRegStr HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "TDB" "$INSTDIR\${MAPNAME}.tdb"
  
; Write uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

; Create uninstaller registry keys
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${REG_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${REG_KEY}" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${REG_KEY}" "NoModify" 1
  
SectionEnd

Section "Uninstall"
; Files to be uninstalled
  Delete "$INSTDIR\${MAPNAME}.img"
  Delete "$INSTDIR\${MAPNAME}_mdr.img"
  Delete "$INSTDIR\${MAPNAME}.mdx"
  Delete "$INSTDIR\joe.typ"
  Delete "$INSTDIR\${MAPNAME}.tdb"
  Delete "$INSTDIR\71221559.img"
  Delete "$INSTDIR\71221560.img"
  Delete "$INSTDIR\71221561.img"
  Delete "$INSTDIR\71221562.img"
  Delete "$INSTDIR\71221563.img"
  Delete "$INSTDIR\71221564.img"
  Delete "$INSTDIR\71221565.img"
  Delete "$INSTDIR\71221566.img"
     Delete "$INSTDIR\71221567.img"
   Delete "$INSTDIR\71221568.img"
   Delete "$INSTDIR\71221569.img"
    Delete "$INSTDIR\71221570.img"
	Delete "$INSTDIR\71221571.img"
	Delete "$INSTDIR\71221572.img"
	Delete "$INSTDIR\71221573.img"
 

  Delete "$INSTDIR\Uninstall.exe"

  RmDir "$INSTDIR"

; Registry cleanup
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "ID"
!ifdef INDEX  
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "IDX"
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "MDR"
!endif
!ifdef TYPNAME  
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}" "TYP"
!endif
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "BMAP"
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "LOC"
  DeleteRegValue HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}" "TDB"
  DeleteRegKey /IfEmpty HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}\${PRODUCT_ID}"
  DeleteRegKey /IfEmpty HKLM "SOFTWARE\Garmin\MapSource\Families\${REG_KEY}"
  
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${REG_KEY}"

SectionEnd
